import React from 'react';

interface LogoProps {
    handleLogoChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  }

function Logo({ handleLogoChange }:LogoProps) {
  return (
    <div className='form-group'>
      <label htmlFor="logo">Logo</label>
      <input 
        id='logo'
        className='form-control' 
        style={{color: 'transparent', width: '105px'}}
        type='file' 
        onChange={handleLogoChange} 
        accept='image/png' />
    </div>
  );
}

export default Logo;

import React from 'react';

interface SharkNameProps {
    name: string;
    index: number;
    handleSharkNameChange: (e: React.ChangeEvent<HTMLInputElement>, index: number) => void;
    handleAddSharkName: (e: React.KeyboardEvent, index?: number) => void;

  }

function SharkName({ name, index, handleSharkNameChange, handleAddSharkName }:SharkNameProps) {
  return (
    <div className='col form-group'>
      <label htmlFor="sharkNameInput">Shark Name</label>
      <input
        className='form-control'
        id='sharkNameInput'
        type="text"
        placeholder='Enter shark name'
        value={name}
        onChange={(e) => handleSharkNameChange(e, index)}
        onKeyDown={(e) => handleAddSharkName(e, index)} 

      />
    </div>
  );
}

export default SharkName;

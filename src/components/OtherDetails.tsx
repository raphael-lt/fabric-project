import React from 'react';

interface OtherDetailsProps {
  otherDetails: string;
  handleOtherDetailsChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleAddOtherDetails: (event: React.KeyboardEvent) => void;
}

function OtherDetails({ otherDetails, handleOtherDetailsChange, handleAddOtherDetails }: OtherDetailsProps) {
  return (
    <div className="col">
      <label htmlFor="otherDetails">Other Details</label>
      <input
        className='form-control'
        id='otherDetails'
        type="text"
        placeholder="2ND EVENT | 1 ON 1"
        value={otherDetails}
        onChange={handleOtherDetailsChange}
        onKeyDown={handleAddOtherDetails}
      />
    </div>
  );
}

export default OtherDetails;

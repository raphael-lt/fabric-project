import React from 'react';

interface SharkShortNameProps {
    name: string;
    index: number;
    handleSharkShortNameChange: (e: React.ChangeEvent<HTMLInputElement>, index: number) => void;
    handleAddSharkShortName: (e: React.KeyboardEvent, index?: number) => void;

  }

function SharkShortName({ name, index, handleSharkShortNameChange, handleAddSharkShortName }:SharkShortNameProps) {
  return (
    <div className='col form-group'>
      <label htmlFor="sharShortkNameInput">Shark Short Name</label>
      <input
        className='form-control'
        id='sharShortkNameInput'
        type="text"
        placeholder='Enter shark short name'
        value={name}
        onChange={(e) => handleSharkShortNameChange(e, index)}
        onKeyDown={(e) => handleAddSharkShortName(e, index)} 
      />
    </div>
  );
}

export default SharkShortName;

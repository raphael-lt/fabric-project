import React from 'react';

interface RaceProps {
  race: string;
  handleRaceChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleAddRace: (event: React.KeyboardEvent) => void;
}

function Race({ race, handleRaceChange, handleAddRace }: RaceProps) {
  return (
    <div className="col">
      <label htmlFor="raceInput">Race</label><br />
      <input
        className='form-control'
        id='raceInput'
        min="1"
        type="number"
        placeholder="21"
        value={race}
        onChange={handleRaceChange}
        onKeyDown={handleAddRace}
      />
    </div>
  );
}

export default Race;

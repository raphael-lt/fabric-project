import React from 'react';

interface EventNameProps {
  eventName: string;
  handleEventNameChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleAddEventName: (event: React.KeyboardEvent) => void;
}

function EventName({ eventName, handleEventNameChange, handleAddEventName }: EventNameProps) {
  return (
    <div className='col form-group'>
      <label htmlFor="eventNameInput">Event Name</label>
      <input
        className='form-control'
        id='eventNameInput'
        type="text"
        placeholder="Team D VS A"
        value={eventName}
        onChange={handleEventNameChange}
        onKeyDown={handleAddEventName}
      />
    </div>
  );
}

export default EventName;

import React, { useEffect, useState, useRef } from 'react';
import { fabric } from 'fabric';

// Check if an object with the same name already exists (ex. prevents event name to be entered twice)
function isObjectExists(canvas: fabric.Canvas | null, objName: string): boolean {
  if (!canvas) {
    return false;
  }

  const existingObj = canvas.getObjects().find((obj) => {
    if (obj instanceof fabric.Text) {
      return obj.text === objName;
    }
    return false;
  });

  return !!existingObj;
}

export const FabricCanvas = () => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const canvas = useRef<fabric.Canvas | null>(null);
  const [backgroundImage, setBackgroundImage] = useState<fabric.Image | null>(null);
  const [eventName, setEventName] = useState<string>('');
  const [gameType, setGameType] = useState<string>('');
  const [race, setRace] = useState<string>('');
  const [date, setDate] = useState<string>('');
  const [arena, setArena] = useState<string>(''); 

  useEffect(() => {
    if (!canvasRef.current) return;

    // Initialize a Fabric.js canvas
    canvas.current = new fabric.Canvas(canvasRef.current, {
      selection: true,
      selectionBorderColor: 'blue',
      selectionLineWidth: 2,
    });

    // Enable image resizing (scaling)
    canvas.current?.on('object:scaling', (e: fabric.IEvent) => {
      const obj = e.target;
      if (obj instanceof fabric.Image) {
        obj.setCoords();
      }
    });

    return () => {
      canvas.current?.dispose();
    };

  }, [canvasRef]);

  const handleBackgroundUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];

    if (file && canvas.current) {
      const reader = new FileReader();
      reader.onload = (event) => {
        const imageUrl = event.target?.result as string;

        if (imageUrl) {
          // Clear the previous background image, if it exists
          if (backgroundImage) {
            canvas.current?.remove(backgroundImage);
          }

          fabric.Image.fromURL(imageUrl, (img) => {
            img.set({
              left: 0,
              top: 0,
              selectable: false,
              evented: false,
            });
            setBackgroundImage(img);
            canvas.current?.add(img);
            canvas.current?.sendToBack(img);
            canvas.current?.renderAll();
          });
        }
      };
  
      reader.readAsDataURL(file);
    }
  };

  const handleImageUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
  
    if (file && canvas.current) {
      const reader = new FileReader();
      reader.onload = (event) => {
        const imageUrl = event.target?.result as string;
  
        if (imageUrl) {
          fabric.Image.fromURL(imageUrl, (img) => {
            canvas.current?.add(img);
            img.set({
              left: 0,
              top: 0,
            });
            canvas.current?.renderAll();
          });
        }
      };
  
      reader.readAsDataURL(file);
    }
  };

  const updateCanvasText = (text: string) => {
    if (canvas.current) {
      canvas.current.forEachObject((obj) => {
        if (obj instanceof fabric.Text) {
          obj.set({ text });
        }
      });
      canvas.current.renderAll();
    }
  };

  const handleEventNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEventName(e.target.value);
    updateCanvasText(e.target.value);
  };
  
  const handleAddEventName = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter' && eventName && canvas.current) {
      const isExistingObj = isObjectExists(canvas.current, eventName);
  
      if (!isExistingObj) {
        const textObject = new fabric.Text(eventName, {
          left: 0,
          top: 0,
          fontWeight: 'bold',
        });
        canvas.current.add(textObject);
        canvas.current.renderAll();
      }
    }
  };
  
  const handleGameTypeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setGameType(e.target.value);
    updateCanvasText(e.target.value);
  };

  const handleAddGameType = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter' && gameType && canvas.current) {
      const isExistingObj = isObjectExists(canvas.current, gameType);

      if (!isExistingObj) {
        const textObject = new fabric.Text(gameType, {
          left: 0,
          top: 0,
        });
        canvas.current.add(textObject);
        canvas.current.renderAll();
      }
    }
  };

  const handleRaceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRace(e.target.value);
    updateCanvasText(e.target.value);
  };

  const handleAddRace = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter' && race && canvas.current) {
      const isExistingObj = isObjectExists(canvas.current, race);

      if (!isExistingObj) {
        const textObject = new fabric.Text(race, {
          left: 0,
          top: 0,
        });
        canvas.current.add(textObject);
        canvas.current.renderAll();
      }
    }
  };

  const formatCustomDate = (dateString: string) => {
    const dateObj = new Date(dateString);
    return dateObj.toLocaleDateString(undefined, { weekday: 'long', month: 'long', day: 'numeric', year: 'numeric' });
  };

  const handleDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDate(formatCustomDate(e.target.value));
    updateCanvasText(formatCustomDate(e.target.value));
  };

  const handleAddDate = () => {
    if (date && canvas.current) {
      const formattedDate = formatCustomDate(date);
      const isExistingObj = isObjectExists(canvas.current, formattedDate);

      if (!isExistingObj) {
        const dateObject = new fabric.Text(formattedDate, {
          left: 0,
          top: 50,
        });
        canvas.current.add(dateObject);
        canvas.current.renderAll();
      }
    }
  };

  const handleArenaSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const isExistingObj = isObjectExists(canvas.current, arena);
    setArena(e.target.value);
    updateCanvasText(e.target.value);
    
    if (!isExistingObj) {
      if (e.target.value && canvas.current) {
        const itemObject = new fabric.Text(e.target.value, {
          left: 0,
          top: 0,
          fontWeight: 'normal',
        });
        canvas.current.add(itemObject);
        canvas.current.renderAll();
      }
    }
  };

  const handleDeleteObject = () => {
    if (canvas.current) {
      const selectedObject = canvas.current.getActiveObject();
      if (selectedObject) {
        canvas.current.remove(selectedObject);
        canvas.current.discardActiveObject();
        canvas.current.renderAll();
      }
    }
  };

  return (
    <div>
      {/* <input type="file" onChange={handleImageUpload} accept="image/*" />
      <input type="file" onChange={handleBackgroundUpload} accept="image/*" /> */}
      <div className='container my-2' style={{height: "80%"}}>
        <div className='d-flex justify-content-center'>
          <canvas className='border border-secondary rounded' ref={canvasRef} width="900" height="506"/>
        </div>
      </div>
      <div className='container w-'>
        <div className='row d-flex align-items-start text-start'>
          <div className='col form-group'>
            <label htmlFor="eventNameInput">Event Name</label>
            <input
              className='form-control'
              id='eventNameInput'
              type="text"
              placeholder="Team D VS A"
              value={eventName}
              onChange={handleEventNameChange}
              onKeyPress={handleAddEventName}
            />
          </div>
          <div className="col">
            <label htmlFor="gameTypeInput">Game Type</label>
            <input
              className='form-control'
              id='gameTypeInput'
              type="text"
              placeholder="9 balls"
              value={gameType}
              onChange={handleGameTypeChange}
              onKeyPress={handleAddGameType}
            />
          </div>
          <div className="col">
            <label htmlFor="raceInput">Race</label><br />
            <input
              className='form-control'
              id='raceInput'
              type="number"
              placeholder="21"
              value={race}
              onChange={handleRaceChange}
              onKeyPress={handleAddRace}
            />
          </div>
          <div className="col">
            <label htmlFor="dateSelect">Date</label><br />
            <input
              className='form-control'
              id='dateSelect'
              type="date"
              value={date}
              onChange={handleDateChange}
              onKeyPress={handleAddDate}
            />
          </div>
          <div className="col">
            <label htmlFor="arenaSelect">Arena</label><br />
            <select
              className='form-control'
              id='arenaSelect'
              value={arena}
              onChange={handleArenaSelect}
            >
              <option hidden>Select Arena</option>
              <option value="Tiger Arena">Tiger Arena</option>
              <option value="Great White Arena">Great White Arena</option>
            </select>
          </div>
        </div>
      </div>
      <button onClick={handleDeleteObject}>Delete Selected Object</button>
    </div>
  );
};

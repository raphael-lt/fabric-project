import React from 'react';

interface DateProps {
  date: string;
  handleDateChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleAddDate: (event: React.MouseEvent) => void;
}

function DateInput({ date, handleDateChange, handleAddDate }: DateProps) {
  return (
    <div className="col">
      <label htmlFor="dateSelect">Date</label><br />
      <input
        className='form-control'
        id='dateSelect'
        type="date"
        value={date}
        onChange={handleDateChange}
        onClick={handleAddDate}
      />
    </div>
  );
}

export default DateInput;

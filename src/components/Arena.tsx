import React from 'react';

interface ArenaProps {
  arena: string;
  handleArenaSelect: (event: React.ChangeEvent<HTMLSelectElement>) => void;
}

function Arena({ arena, handleArenaSelect }: ArenaProps) {
  return (
    <div className="col-3">
      <label htmlFor="arenaSelect">Arena</label><br />
      <select
        className='form-select'
        id='arenaSelect'
        value={arena}
        onChange={handleArenaSelect}
      >
        <option hidden>Select Arena</option>
        <option value="SHARKS TIGER ARENA">SHARKS TIGER ARENA</option>
        <option value="SHARKS GREAT WHITE ARENA">SHARKS GREAT WHITE ARENA</option>
      </select>
    </div>
  );
}

export default Arena;

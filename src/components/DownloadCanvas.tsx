import React from 'react';

interface DownloadCanvasProps {
  canvasRef: React.MutableRefObject<fabric.Canvas | null>;
}

const DownloadCanvas: React.FC<DownloadCanvasProps> = ({ canvasRef }) => {
  const downloadCanvasImage = () => {
    if (!canvasRef.current) return;

    const timestamp = new Date().toISOString().replace(/[-:.TZ]/g, '');
    const dataURL = canvasRef.current.toDataURL({
      format: 'png',
      multiplier: 2,  // Adjust for higher resolution
    });

    const link = document.createElement('a');
    link.href = dataURL;
    link.download = `event_poster_${timestamp}.png`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <button className='btn btn-primary' onClick={downloadCanvasImage} style={{float: 'right'}}>
      Download Canvas
    </button>
  );
};

export default DownloadCanvas;
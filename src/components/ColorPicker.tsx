import React from 'react';
import { SketchPicker, ColorResult } from 'react-color';

interface ColorPickerProps {
  color: string;
  onChange: (color: string) => void;
}

const ColorPicker: React.FC<ColorPickerProps> = ({ color, onChange }) => {
  return (
    <div className='mb-3'>
      <SketchPicker
        color={color}
        onChange={(colorResult: ColorResult) => {
          const rgbaColor = `rgba(${colorResult.rgb.r}, ${colorResult.rgb.g}, ${colorResult.rgb.b}, ${colorResult.rgb.a})`;
          onChange(rgbaColor);
        }}
      />
    </div>
  );
};

export default ColorPicker;

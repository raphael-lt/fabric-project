import React from 'react';

interface BackgroundImageProps {
    handleBackgroundImageChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  }

function BackgroundImage({ handleBackgroundImageChange }:BackgroundImageProps) {
  return (
    <div className='form-group'>
      <label htmlFor="backgroundImage">Background</label>
      <input 
        id='backgroundImage'
        className='form-control' 
        style={{color: 'transparent', width: '105px'}}
        type='file' 
        onChange={handleBackgroundImageChange} 
        accept='image/png' />
    </div>
  );
}

export default BackgroundImage;

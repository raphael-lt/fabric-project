import React, { useEffect, useRef } from 'react';
import { fabric } from 'fabric';

interface FabricCanvasProps {
  canvasRef: React.MutableRefObject<fabric.Canvas | null>;
}

function FabricCanvas({ canvasRef }: FabricCanvasProps) {
  const canvasElementRef = useRef<HTMLCanvasElement | null>(null);

  useEffect(() => {
    if (!canvasElementRef.current) return;

    // Initialize a Fabric.js canvas
    const canvas = new fabric.Canvas(canvasElementRef.current, {
      selection: true,
      selectionBorderColor: 'blue',
      selectionLineWidth: 2,
      preserveObjectStacking: true 
    });

    // Assign the canvas to the provided ref
    canvasRef.current = canvas;

    // Handle fabric objects not get dragged out of canvas
    canvas.on('object:moving', (e) => {
      const obj = e.target as fabric.Object;

      if (!obj || typeof obj.height === 'undefined' || typeof obj.width === 'undefined') {
        return;
      }

      const offsetWidth = obj.getBoundingRect().width * 0.2;
      const offsetHeight = obj.getBoundingRect().height * 0.2;

      obj.setCoords();

      if (typeof obj.left !== 'undefined' && typeof obj.top !== 'undefined' &&
        obj.getBoundingRect().top < -offsetHeight || obj.getBoundingRect().left < -offsetWidth) {
        obj.top = Math.max((obj.top || 0), (obj.top || 0) - (obj.getBoundingRect().top + offsetHeight));
        obj.left = Math.max((obj.left || 0), (obj.left || 0) - (obj.getBoundingRect().left + offsetWidth));
      }

      if (typeof obj.left !== 'undefined' && typeof obj.top !== 'undefined' &&
        obj.getBoundingRect().top + obj.getBoundingRect().height > (canvas.height || 0) + offsetHeight ||
        obj.getBoundingRect().left + obj.getBoundingRect().width > (canvas.width || 0) + offsetWidth) {
        obj.top = Math.min((obj.top || 0), (canvas.height || 0) - obj.getBoundingRect().height + (obj.top || 0) - obj.getBoundingRect().top + offsetHeight);
        obj.left = Math.min((obj.left || 0), (canvas.width || 0) - obj.getBoundingRect().width + (obj.left || 0) - obj.getBoundingRect().left + offsetWidth);
      }
    });

    return () => {
      canvas.dispose();
    };
  }, [canvasRef]);

  return (
    <div className='container my-2' style={{ height: '80%' }}>
      <div className='d-flex justify-content-end'>
        <canvas className='border border-secondary rounded' ref={canvasElementRef} width="900" height="506" />
      </div>
    </div>
  );
}

export default FabricCanvas;

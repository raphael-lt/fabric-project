import React from 'react';

interface SharkShortNameProps {
    index: number;
    handleSharkImageChange: (e: React.ChangeEvent<HTMLInputElement>, index: number) => void;
  }

function SharkImage({ index, handleSharkImageChange }:SharkShortNameProps) {
  return (
    <div className='form-group'>
      <label htmlFor="sharkImage">Shark Image</label>
      <input 
        className='form-control' 
        style={{color: 'transparent', width: '105px'}}
        type='file' 
        onChange={(e) => handleSharkImageChange(e, index)} 
        accept='image/png' />
    </div>
  );
}

export default SharkImage;

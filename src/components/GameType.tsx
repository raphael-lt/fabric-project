import React from 'react';

interface GameTypeProps {
  gameType: string;
  handleGameTypeChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleAddGameType: (event: React.KeyboardEvent) => void;
}

function GameType({ gameType, handleGameTypeChange, handleAddGameType }: GameTypeProps) {
  return (
    <div className="col">
      <label htmlFor="gameTypeInput">Game Type</label>
      <input
        className='form-control'
        id='gameTypeInput'
        type="text"
        placeholder="9 balls"
        value={gameType}
        onChange={handleGameTypeChange}
        onKeyDown={handleAddGameType}
      />
    </div>
  );
}

export default GameType;

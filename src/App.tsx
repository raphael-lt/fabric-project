import React, { useState, useRef, useEffect } from 'react';
import FabricCanvas from './components/FabricCanvas';
import EventName from './components/EventName';
import GameType from './components/GameType';
import Race from './components/Race';
import DateInput from './components/DateInput';
import Arena from './components/Arena';
import BackgroundImage from './components/BackgroundImage';
import SharkName from './components/SharkName';
import SharkShortName from './components/SharkShortName';
import SharkImage from './components/SharkImage';
import ColorPicker from './components/ColorPicker';
import DownloadCanvas from './components/DownloadCanvas';
import { isObjectExists, updateCanvasText, updateCanvasTextGroup, isImageInGroup, isObjectExistsGroup } from './helpers/Helpers';
import { handleBringToFront, handleGroupObjects, handleSendToBack, handleTextColorChange, handleUngroupObjects } from './helpers/ObjectControllers';
import { fabric } from 'fabric';
import Logo from './components/Logo';
import OtherDetails from './components/OtherDetails';

function App() {
  const canvasRef = useRef<fabric.Canvas | null>(null);
  const [eventName, setEventName] = useState<string>('');
  const [gameType, setGameType] = useState<string>('');
  const [race, setRace] = useState<string>('');
  const [date, setDate] = useState<string>('');
  const [displayDate, setDisplayDate] = useState<string>('');
  const [arena, setArena] = useState<string>('');
  const [background, setBackground] = useState<string>('');
  const [logo, setLogo] = useState<string>('');
  const [otherDetails, setOtherDetails] = useState<string>('');
  const [sharks, setSharks] = useState<Array<{ sharkName: string; sharkImage: File | null; sharkShortName: string; sharkNameObj: fabric.Text | null; sharkImageObj: fabric.Image | null; sharkShortNameObj: fabric.Text | null; textGroupObj: fabric.Group | null; isInitialized: boolean; isTextsInitialized: boolean }>>([{ sharkName: '', sharkImage: null, sharkShortName: '', sharkNameObj: null, sharkImageObj: null, sharkShortNameObj: null, textGroupObj: null, isInitialized: false, isTextsInitialized: false }]);
  const [textColor, setTextColor] = useState<string>('#000000');
  
  useEffect(() => {
    sharks.forEach(sharkObj => {
      if (sharkObj.sharkNameObj !== null && sharkObj.sharkShortNameObj !== null) {
        if (!sharkObj.isTextsInitialized) {
          sharkObj.isTextsInitialized = true;

          const texts = new fabric.Group([sharkObj.sharkNameObj, sharkObj.sharkShortNameObj]);
          const backgroundRect = new fabric.Rect({
            width: (texts.width || 0)+20,
            height: (texts.height || 0)+30,
            fill: '#000000',
            originY: 'top',
            top: texts.getBoundingRect().top-10,
            left: texts.getBoundingRect().left-10,
            selectable: false,
            evented: false,
          });
  
          texts.addWithUpdate(backgroundRect); 
          backgroundRect.moveTo(0);
          canvasRef.current?.add(texts);
          canvasRef.current?.renderAll();
          canvasRef.current?.setActiveObject(texts);

          canvasRef.current?.remove(backgroundRect);
          canvasRef.current?.remove(sharkObj.sharkNameObj);
          canvasRef.current?.remove(sharkObj.sharkShortNameObj);
          canvasRef.current?.requestRenderAll();
          canvasRef.current?.renderAll();

          sharkObj.textGroupObj = texts;
        }
      }

      if (!sharkObj.isInitialized && sharkObj.isTextsInitialized && sharkObj.sharkImageObj !== null && sharkObj.sharkNameObj !== null && sharkObj.sharkShortNameObj !== null) {
        sharkObj.isInitialized = true;

        const sharkTextGroup = canvasRef.current?.getObjects().find((obj) => obj === sharkObj.textGroupObj);
        if (sharkTextGroup) {
          canvasRef.current?.remove(sharkTextGroup);
          const group = new fabric.Group([sharkObj.sharkImageObj, sharkTextGroup]);
          canvasRef.current?.add(group);
          canvasRef.current?.renderAll();
          canvasRef.current?.setActiveObject(group);

          canvasRef.current?.remove(sharkObj.sharkImageObj);
          canvasRef.current?.requestRenderAll();
          canvasRef.current?.renderAll();
        }
      }
    });

    if (sharks.length === 0) {
      toggleSharkInputs();
    }
  }, [sharks])

  const toggleSharkInputs = () => {
    setSharks((prevSharks) => [
      ...prevSharks,
      { sharkName: '', sharkImage: null, sharkShortName: '', sharkNameObj: null, sharkImageObj: null, sharkShortNameObj: null, textGroupObj: null, isInitialized: false, isTextsInitialized: false},
    ]);
  };

  const toggleVSIcon = () => {
    if (canvasRef.current) {
      const vsImage = '/images/vs.png';

      const vsIcon = canvasRef.current?.getObjects().find((obj) => obj.name === `vsIcon`);
      if (vsIcon instanceof fabric.Image) {
        canvasRef.current?.remove(vsIcon);
      }

      fabric.Image.fromURL(vsImage, (image) => {
        const canvasWidth = canvasRef.current?.width || 0;
        const canvasHeight = canvasRef.current?.height || 0;
        const scaleFactor = canvasHeight * 0.23 / (image.height || 1);

        const newWidth = (image.width || 0) * scaleFactor;
        const newHeight = (image.height || 0) * scaleFactor;

        image.set({
          name: `vsIcon`,
          left: (canvasWidth - newWidth) / 2,
          top: (canvasHeight - newHeight) / 2,
          scaleX: newWidth / (image.width || 1),
          scaleY: newHeight / (image.height || 1),
          selectable: false,
          evented: false,
        });

        canvasRef.current?.add(image);
        canvasRef.current?.renderAll();
      });
    }
  }

  // =========== SHARK IMAGE =========== //
  
  const handleSharkImageChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    if (!canvasRef.current) return;
  
    const handleImage = (file: File) => {
      setSharks((prevSharks) =>
        prevSharks.map((shark, i) => (i === index ? { ...shark, sharkImage: file } : shark))
      );
  
      const sharkImageObj = canvasRef.current?.getObjects().find((obj) => obj.name === `sharkImage${index}`);
  
      if (sharkImageObj instanceof fabric.Image) {
        canvasRef.current?.remove(sharkImageObj);
      }
  
      fabric.Image.fromURL(URL.createObjectURL(file), (image) => {
        const canvasWidth = canvasRef.current?.width || 0;
        const canvasHeight = canvasRef.current?.height || 0;
        const scaleFactor = canvasHeight * (3 / 4) / (image.height || 0);
  
        const newWidth = (image.width || 0) * scaleFactor;
        const newHeight = (image.height || 0) * scaleFactor;
        const left = (canvasWidth - newWidth) / 2;
        const top = (canvasHeight - newHeight) / 2;
  
        image.set({
          name: `sharkImage${index}`,
          left: left,
          top: top,
          scaleX: newWidth / (image.width || 1),
          scaleY: newHeight / (image.height || 1),
        });
  
        setSharks((prevSharks) => {
          const updatedSharks = [...prevSharks];
          updatedSharks[index] = {...updatedSharks[index], sharkImageObj: image,};
  
          return updatedSharks;
        });  

        canvasRef.current?.add(image);
        canvasRef.current?.renderAll();
      });
    };
  
    if (isImageInGroup(canvasRef.current, `sharkImage${index}`)) {
      handleUngroupObjects(canvasRef);
      const sharkImageObj = canvasRef.current.getObjects().find((obj) => obj.name === `sharkImage${index}`);
      if (sharkImageObj instanceof fabric.Image) {
        canvasRef.current.remove(sharkImageObj);
      }
    }
  
    if (e.target.files && e.target.files[0]) {
      handleImage(e.target.files[0]);
    }
  };

  // =========== SHARK NAME =========== //

  const handleSharkNameChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (!activeObject && isObjectExistsGroup(canvasRef.current, sharks[index].sharkName)) return;

    setSharks((prevSharks) =>
      prevSharks.map((shark, i) =>
        i === index ? { ...shark, sharkName: e.target.value } : shark
      )
    );
  
    if (!canvasRef.current) return; 

    // Check if the object is a group
    if (activeObject && activeObject.type === 'group') {
      const group = activeObject as fabric.Group;
      const textObj = group.getObjects().find((obj) => obj.name === 'sharkName' + index.toString());
  
      // Check if the text object is found in the group
      if (textObj instanceof fabric.Text) {
        textObj.set({ text: e.target.value });
        canvasRef.current.renderAll();
      }
    } else {
      // If the object is not a group, find the text object by name and update the text
      const textObj = canvasRef.current.getObjects().find((obj) => obj.name === 'sharkName' + index.toString());
  
      // Check if the text object is found outside the group
      if (textObj instanceof fabric.Text) {
        textObj.set({ text: e.target.value });
        canvasRef.current.renderAll();
      }
    }
  };

  const handleAddSharkName = (e: React.KeyboardEvent, index: number) => {
    if (!canvasRef.current) return;

    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;

    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, sharks[index].sharkName);
    const isExistingObj = canvasRef.current.getObjects().some((obj) => obj.name === `sharkName${index}`);

    if (!isExistingObj && !isExistingObjGroup) {
      const textObject = new fabric.Text(sharks[index].sharkName, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2 +35,
        originX: 'right',
        originY: 'center',
        textAlign: 'center',
        backgroundColor: '#000000',
        fill: '#ffffff',
        name: `sharkName${index}`,
        fontSize: 28,
        fontFamily: 'big_noodle_titling',
      });

      setSharks((prevSharks) => {
        const updatedSharks = [...prevSharks];
        updatedSharks[index] = {...updatedSharks[index], sharkNameObj: textObject,};

        return updatedSharks;
      });  

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  // =========== SHARK SHORT NAME =========== //

  const handleSharkShortNameChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (!activeObject && isObjectExistsGroup(canvasRef.current, sharks[index].sharkShortName)) return;

    setSharks((prevSharks) =>
      prevSharks.map((shark, i) =>
        i === index ? { ...shark, sharkShortName: e.target.value } : shark
      )
    );

    if (!canvasRef.current) return; 
  
    if (activeObject && activeObject.type === 'group') {
      const group = activeObject as fabric.Group;
      const textObj = group.getObjects().find((obj) => obj.name === 'sharkShortName' + index.toString());
  
      if (textObj instanceof fabric.Text) {
        textObj.set({ text: e.target.value });
        canvasRef.current.renderAll();
      }
    } else {
      const textObj = canvasRef.current.getObjects().find((obj) => obj.name === 'sharkShortName' + index.toString());
  
      if (textObj instanceof fabric.Text) {
        textObj.set({ text: e.target.value });
        canvasRef.current.renderAll();
      }
    }
  };

  const handleAddSharkShortName = (e: React.KeyboardEvent, index: number) => {
    if (!canvasRef.current) return;

    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;

    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, sharks[index].sharkShortName);
    const isExistingObj = canvasRef.current.getObjects().some((obj) => obj.name === `sharkShortName${index}`);

    if (!isExistingObj && !isExistingObjGroup) {
      const textObject = new fabric.Text(sharks[index].sharkShortName, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'right',
        originY: 'center',
        textAlign: 'center',
        backgroundColor: '#000000',
        fill: '#ffffff',
        name: `sharkShortName${index}`,
        fontFamily: 'big_noodle_titling',
      });

      setSharks((prevSharks) => {
        const updatedSharks = [...prevSharks];
        updatedSharks[index] = {...updatedSharks[index], sharkShortNameObj: textObject,};

        return updatedSharks;
      });  

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };
  
  // =========== SHARK DELETE =========== //

  const handleSharkDelete = (index: number) => {
    if (!canvasRef.current) return;

    const deleteSharkObjects = (group: fabric.Group, obj: fabric.Object, index: number) => {
      if (obj === sharks[index].sharkImageObj) {
        canvasRef.current?.remove(obj);
        group.remove(obj);
      }

      if (obj === sharks[index].sharkNameObj) {
        canvasRef.current?.remove(obj);
        group.remove(obj);
      }

      if (obj === sharks[index].sharkShortNameObj) {
        canvasRef.current?.remove(obj);
        group.remove(obj);
      }

      if (obj === sharks[index].textGroupObj) {
        canvasRef.current?.remove(obj);
        group.remove(obj);
      }
    }

    // Container of group objects
    const containerObj = canvasRef.current.getObjects().filter((obj) => obj.type === 'group');

    if (containerObj.length >= 1) {
      containerObj.forEach((groupObj) => {  // Loops through group container (usually has 1 object)
        if (groupObj instanceof fabric.Group) {  // If group container has group object
          groupObj.getObjects().forEach((obj) => {  // Loops through group objects
            if (obj instanceof fabric.Group) {  // If group object has an object of type group
              obj.getObjects().forEach((innerObj) => {

                canvasRef.current?.setActiveObject(groupObj);
                handleUngroupObjects(canvasRef);
                deleteSharkObjects(obj, innerObj, index);
              })
            }

            deleteSharkObjects(groupObj, obj, index)
          })
        }

        if (groupObj instanceof fabric.Group && groupObj._objects.length === 0)
          canvasRef.current?.remove(groupObj)
      });
    }
    
    const sharkNameObj = canvasRef.current.getObjects().find((obj) => obj === sharks[index].sharkNameObj);
    const sharkImageObj = canvasRef.current.getObjects().find((obj) => obj === sharks[index].sharkImageObj);
    const sharkShortNameObj = canvasRef.current.getObjects().find((obj) => obj === sharks[index].sharkShortNameObj);
    const textGroupObj = canvasRef.current.getObjects().find((obj) => obj === sharks[index].textGroupObj);

    if (sharkNameObj) canvasRef.current.remove(sharkNameObj);
    if (sharkShortNameObj) canvasRef.current.remove(sharkShortNameObj);
    if (sharkImageObj) canvasRef.current.remove(sharkImageObj);
    if (textGroupObj) canvasRef.current.remove(textGroupObj);
    
    setSharks((prevSharks) => prevSharks.filter((sharkObj, i) => i !== index));

    canvasRef.current.discardActiveObject();
    canvasRef.current.renderAll();
  };

  // =========== EVENT NAME =========== //

  // Add new eventName
  const handleAddEventName = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;

    // Check if eventName object is in a group
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;

    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, eventName)
    const isExistingObj = isObjectExists(canvasRef.current, eventName);

    if (!isExistingObj && !isExistingObjGroup) {
      const textObject = new fabric.Text(eventName, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'eventName',
        fontWeight: 'bold',
        fontFamily: 'big_noodle_titling',
      });

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  // eventName onChange handler
  const handleEventNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;

    if (!isObjectExistsGroup(canvasRef.current, eventName)) {
      updateCanvasText(canvasRef, e.target.value, 'eventName')
      setEventName(e.target.value);
    } else {
      try {
        if (activeObject.getObjects().find((item) => item.name === 'eventName')) {
          setEventName(e.target.value);
          updateCanvasTextGroup(canvasRef, e.target.value, 'eventName')
        }
      } catch (error) {
      }
    }
  };

  // =========== GAME TYPE =========== //

  const handleAddGameType = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;

    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;

    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, gameType.toUpperCase());
    const isExistingObj = isObjectExists(canvasRef.current, gameType.toUpperCase());

    if (!isExistingObj && !isExistingObjGroup) {
      const textObject = new fabric.Text(gameType.toUpperCase(), {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'gameType',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });
      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  const handleGameTypeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;

    if (!isObjectExistsGroup(canvasRef.current, gameType.toUpperCase())) {
      updateCanvasText(canvasRef, e.target.value.toUpperCase(), 'gameType')
      setGameType(e.target.value);
    } else {
      try {
        if (activeObject.getObjects().find((item) => item.name === 'gameType')) {
          setGameType(e.target.value);
          updateCanvasTextGroup(canvasRef, e.target.value.toUpperCase(), 'gameType')
        }
      } catch (error) {
      }
    }
  };

  // =========== RACE =========== //

  const handleAddRace = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;

    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;

    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, `RACE TO ${race}`);
    const isExistingObj = isObjectExists(canvasRef.current, `RACE TO ${race}`);

    if (!isExistingObj && !isExistingObjGroup) {
      const textObject = new fabric.Text(`RACE TO ${race.toUpperCase()}`, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'race',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  const handleRaceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    
    if (!isObjectExistsGroup(canvasRef.current, `RACE TO ${race}`)) {
      updateCanvasText(canvasRef, `RACE TO ${e.target.value}`, 'race')
      setRace(e.target.value);
    } else {
      try {
        if (activeObject.getObjects().find((item) => item.name === 'race')) {
          setRace(e.target.value);
          updateCanvasTextGroup(canvasRef, `RACE TO ${e.target.value}`, 'race')
        }
      } catch (error) {
      }
    }
  };

  // =========== DATE =========== //

  const formatCustomDate = (dateString: string) => {
    const dateObj = new Date(dateString);
    return dateObj.toLocaleDateString(undefined, { weekday: 'long', month: 'long', day: 'numeric', year: 'numeric' }).toUpperCase();
  };

  const handleAddDate = () => {
    if (!canvasRef.current) return;

    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;
    
    const formattedDate = formatCustomDate(date);
    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, formattedDate);
    const isExistingObj = isObjectExists(canvasRef.current, formattedDate);

    if (!isExistingObj && !isExistingObjGroup) {
      const dateObject = new fabric.Text(formattedDate, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'date',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });

      canvasRef.current.add(dateObject);
      canvasRef.current.renderAll();
    }
  };

  const handleDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;

    if (!isObjectExistsGroup(canvasRef.current, date)) {
      updateCanvasText(canvasRef, formatCustomDate(e.target.value), 'date')
      setDisplayDate(e.target.value);
      setDate(formatCustomDate(e.target.value));
    } else {
      try {
        if (activeObject.getObjects().find((item) => item.name === 'date')) {
          setDate(formatCustomDate(e.target.value));
          setDisplayDate(e.target.value);
          updateCanvasTextGroup(canvasRef, formatCustomDate(e.target.value), 'date')
        }
      } catch (error) {
      }
    }
  };

  // =========== ARENA =========== //

  const handleArenaSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
  
    if (!isObjectExistsGroup(canvasRef.current, arena)) {
      const isExistingObj = isObjectExists(canvasRef.current, arena);
      setArena(e.target.value);
      updateCanvasText(canvasRef, e.target.value, 'arena');

      if (isExistingObj) return;

      if (e.target.value && canvasRef.current) {
        const itemObject = new fabric.Text(e.target.value, {
          left: (canvasRef.current.width || 0) / 2,
          top: (canvasRef.current.height || 0) / 2,
          originX: 'center',
          originY: 'center',
          textAlign: 'center',
          name: 'arena',
          text: e.target.value,
          fontSize: 16,
          fontWeight: 'bold',
          fontFamily: 'Century Gothic',
        });

        canvasRef.current.add(itemObject);
        canvasRef.current.renderAll();

      }
    } else {
      try {
        if (activeObject && activeObject.getObjects().find((item) => item.name === 'arena')) {
          setArena(e.target.value);
          updateCanvasTextGroup(canvasRef, e.target.value, 'arena');
        }
      } catch (error) {
        // Handle error if needed
      }
    }
  };

  // =========== BACKGROUND =========== //

  const handleBackgroundImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];

    if (file && canvasRef.current) {
      const reader = new FileReader();

      reader.onload = (event) => {
        const imageUrl = event.target?.result as string;

        if (imageUrl) {
          fabric.Image.fromURL(imageUrl, (img) => {
            if (canvasRef.current) {
              canvasRef.current.setBackgroundImage(img, canvasRef.current.renderAll.bind(canvasRef.current), {
                scaleX: (canvasRef.current.width || 0) / (img.width || 0),
                scaleY: (canvasRef.current.height || 0) / (img.height || 0),
                top: 0,
                left: 0
              }
              );
            }
          });

          setBackground(file.name);
        }
      };

      reader.readAsDataURL(file);
    }
  };

  // =========== LOGO =========== //

  const handleLogoChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];

    if (file && canvasRef.current) {
      const reader = new FileReader();

      const logoObj = canvasRef.current.getObjects().find((obj) => obj.name === 'logo');
      if (logoObj instanceof fabric.Image) {
        canvasRef.current?.remove(logoObj);
      }

      reader.onload = (event) => {
        const imageUrl = event.target?.result as string;

        if (imageUrl) {
          fabric.Image.fromURL(URL.createObjectURL(file), (image) => {
            const canvasWidth = canvasRef.current?.width || 0;
            const canvasHeight = canvasRef.current?.height || 0;
            const scaleFactor = canvasHeight * (0.1) / (image.height || 0);
      
            const newWidth = (image.width || 0) * scaleFactor;
            const newHeight = (image.height || 0) * scaleFactor;
            const left = (canvasWidth - newWidth) / 2;
            const top = (canvasHeight - newHeight) / 2;
      
            image.set({
              name: `logo`,
              left: left,
              top: top,
              fill: '#000000',
              scaleX: newWidth / (image.width || 1),
              scaleY: newHeight / (image.height || 1),
            });

            canvasRef.current?.add(image);
            canvasRef.current?.renderAll();
          });

          setLogo(file.name);
        }
      };

      reader.readAsDataURL(file);
    }
  }

  // =========== OTHER DETAILS =========== //
  
  const handleAddOtherDetails = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;

    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;
    if (activeObject && activeObject.type === 'group') return;

    const isExistingObjGroup = isObjectExistsGroup(canvasRef.current, otherDetails.toUpperCase());
    const isExistingObj = isObjectExists(canvasRef.current, otherDetails.toUpperCase());

    if (!isExistingObj && !isExistingObjGroup) {
      const textObject = new fabric.Text(otherDetails.toUpperCase(), {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'otherDetails',
        fontSize: 20,
        fontFamily: 'big_noodle_titling',
      });
      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  const handleOtherDetailsChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const activeObject = canvasRef.current?.getActiveObject() as fabric.Group;

    if (!isObjectExistsGroup(canvasRef.current, otherDetails.toUpperCase())) {
      updateCanvasText(canvasRef, e.target.value.toUpperCase(), 'otherDetails')
      setOtherDetails(e.target.value);
    } else {
      try {
        if (activeObject.getObjects().find((item) => item.name === 'otherDetails')) {
          setOtherDetails(e.target.value);
          updateCanvasTextGroup(canvasRef, e.target.value.toUpperCase(), 'otherDetails')
        }
      } catch (error) {
      }
    }
  };

  // =========== TEXT COLOR PICKER =========== //

  const handleTextColorChangeWrapper = (color: string) => {
    handleTextColorChange(color, canvasRef);
    setTextColor(color);
  };

  return (
    <div>
      <div className='row d-flex justify-content-center'>
        <div className='col-md-8 pe-0'>
          <FabricCanvas canvasRef={canvasRef} />
        </div>
        <div className='col-md-2 d-flex align-items-start justify-content-center flex-column'>
          <ColorPicker color={textColor} onChange={handleTextColorChangeWrapper} />
          <button className='btn btn-primary mb-2' onClick={(e) => handleBringToFront(canvasRef)}>Bring To Front</button>
          <button className='btn btn-primary mb-2' onClick={(e) => handleSendToBack(canvasRef)}>Send To Back</button>
          <button className='btn btn-primary mb-2' onClick={(e) => handleGroupObjects(canvasRef)}>Group</button>
          <button className='btn btn-primary mb-2' onClick={(e) => handleUngroupObjects(canvasRef)}>Ungroup</button>
        </div>
      </div>
      <div className='container w-'>
        <div className='row mb-2 d-flex align-items-start text-start'>
          <EventName
            eventName={eventName}
            handleEventNameChange={handleEventNameChange}
            handleAddEventName={handleAddEventName}
          />
          <GameType
            gameType={gameType}
            handleGameTypeChange={handleGameTypeChange}
            handleAddGameType={handleAddGameType}
          />
          <Race
            race={race}
            handleRaceChange={handleRaceChange}
            handleAddRace={handleAddRace}
          />
          <DateInput
            date={displayDate}
            handleDateChange={handleDateChange}
            handleAddDate={handleAddDate}
          />
        </div>
        <div className='row mb-2 d-flex justify-content-center'>
          <Arena
            arena={arena}
            handleArenaSelect={handleArenaSelect}
          />
          <div className="col-3 d-flex align-items-end">
            <BackgroundImage handleBackgroundImageChange={handleBackgroundImageChange} />
            <div className='d-flex align-items-start p-2 small'>
              {background ? background : 'No image uploaded'}
            </div>
          </div>
          <div className="col-3 d-flex align-items-end">
            <Logo handleLogoChange={handleLogoChange} />
            <div className='d-flex align-items-start p-2 small'>
              {logo ? logo : 'No image uploaded'}
            </div>
          </div>
          <OtherDetails
            otherDetails={otherDetails}
            handleAddOtherDetails={handleAddOtherDetails}
            handleOtherDetailsChange={handleOtherDetailsChange}
          />
        </div>
        {sharks.map((shark, index) => (
          <div className='row d-flex text-start mb-3'>
            <div className='col-1 d-flex align-items-center justify-content-center'>
              <button className='btn' onClick={(e) => handleSharkDelete(index)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-x-circle-fill" viewBox="0 0 16 16">
                  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                </svg>
            </button>
            </div>
            <div className="col d-flex align-items-end">
              <SharkImage
                key={index}
                index={index}
                handleSharkImageChange={(e) => handleSharkImageChange(e, index)}
              />
              <div className='col d-flex align-items-start p-2 small'>{sharks[index].sharkImage?.name ? sharks[index].sharkImage?.name : 'No image uploaded'}</div>
            </div>
            <div className='col'>
              <SharkName
                key={index}
                index={index}
                name={shark ? shark.sharkName : ''}
                handleSharkNameChange={(e) => handleSharkNameChange(e, index)}
                handleAddSharkName={(e) => handleAddSharkName(e, index)}
              />
            </div>
            <div className="col">
              <SharkShortName
                key={index}
                index={index}
                name={shark ? shark.sharkShortName : ''}
                handleSharkShortNameChange={(e) => handleSharkShortNameChange(e, index)}
                handleAddSharkShortName={(e) => handleAddSharkShortName(e, index)}
              />
            </div>
          </div>
        ))}
        <button className='btn btn-primary' onClick={toggleSharkInputs}>Add More Shark</button>
        <button className='btn btn-primary ms-3' onClick={toggleVSIcon}>Add VS Icon</button>
        <DownloadCanvas canvasRef={canvasRef} />
      </div>
    </div>
  );
};

export default App
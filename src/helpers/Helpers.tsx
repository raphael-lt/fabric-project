import { fabric } from 'fabric';

// Check if an object with the same name already exists (eg., prevents event name to be entered twice)
export function isObjectExists(canvas: fabric.Canvas | null, objName: string): boolean {
  if (!canvas) return false;
  
  const existingObj = canvas.getObjects().find((obj) => {
    if (obj instanceof fabric.Text) {
      return obj.text === objName;
    }
    return false;
  });

  return !!existingObj;
}

export function isObjectExistsGroup(canvas: fabric.Canvas | null, objName: string): boolean {
  if (!canvas) return false;
  
  const existingObjInGroups = canvas.getObjects().some((obj) => {
    if (obj.type === 'group' && obj instanceof fabric.Group) {
      const objectInGroup = obj.getObjects().some((innerObj) => {
        if (innerObj instanceof fabric.Text) {
          return innerObj.text === objName;
        }
        return false;
      });

      return objectInGroup;
    }

    return false;
  });

  return existingObjInGroups;
}

// Updates the text of a canvas object when onChange
export function updateCanvasText(canvasRef: React.MutableRefObject<fabric.Canvas | null>, text: string, objName: string) {
  if (!canvasRef.current) return;

  const textObj = canvasRef.current.getObjects().find((obj) => obj.name === objName);
  if (textObj instanceof fabric.Text) {
    textObj.set({ text });
    canvasRef.current.renderAll();
  }
}

// Update shark objects index (eg., sharkName<index>) based on the updated array (on shark deletion)
export function updateSharkIndex(canvasRef: React.MutableRefObject<fabric.Canvas | null>, sharkObj: string, index: number) {
  if (!canvasRef.current) return;

  canvasRef.current.getObjects().forEach((obj) => {
    if (obj.name && obj.name.startsWith(sharkObj)) {
      const objIndex = parseInt(obj.name.replace(sharkObj, ''), 10);
      if (objIndex > index) {
        const updatedIndex = objIndex - 1;
        obj.set({ name: `${sharkObj}${updatedIndex}` });
      }
    }
  });
}

export function updateCanvasTextGroup(canvasRef: React.MutableRefObject<fabric.Canvas | null>, text: string, objName: string) {
  if (!canvasRef.current) return;

  const activeObject = canvasRef.current.getActiveObject();

  // Check if the active object is a group
  if (activeObject && activeObject.type === 'group') {
    const group = activeObject as fabric.Group; // Cast to fabric.Group
    const textObj = group.getObjects().find((obj) => obj.name === objName);

    // Check if the "eventName" object is found in the group
    if (textObj instanceof fabric.Text) {
      textObj.set({ text });
      canvasRef.current.renderAll();
    }
  } else {
    // If the active object is not a group, find the object by name and update its text
    const textObj = canvasRef.current.getObjects().find((obj) => obj.name === objName);

    // Check if the "eventName" object is found outside the group
    if (textObj instanceof fabric.Text) {
      textObj.set({ text });
      canvasRef.current.renderAll();
    }
  }
}

export const isImageInGroup = (canvas: fabric.Canvas, imageName: string): boolean => {
  const allObjects = canvas.getObjects();

  for (const obj of allObjects) {
    if (obj.type === 'group') {
      const group = obj as fabric.Group;
      const imageInGroup = group.getObjects().find((innerObj) => innerObj.name === imageName);

      if (imageInGroup && imageInGroup.type === 'image') {
        return true;
      }
    }
  }

  return false;
}
import { fabric } from 'fabric';

export const handleBringToFront = (canvasRef: React.MutableRefObject<fabric.Canvas | null>) => {
  if (!canvasRef.current) return;

  const selectedObject = canvasRef.current.getActiveObject();
  if (selectedObject) {
    canvasRef.current.bringForward(selectedObject);
    canvasRef.current.renderAll();
  }
};

export const handleSendToBack = (canvasRef: React.MutableRefObject<fabric.Canvas | null>) => {
  if (!canvasRef.current) return;

  const selectedObject = canvasRef.current.getActiveObject();
  if (selectedObject) {
    canvasRef.current.sendBackwards(selectedObject);
    canvasRef.current.renderAll();
  }
};

export const handleGroupObjects = (canvasRef: React.MutableRefObject<fabric.Canvas | null>) => {
  if (!canvasRef.current) return;

  const activeObject = canvasRef.current.getActiveObject();
  if (!activeObject || activeObject.type !== 'activeSelection') {
    return;
  }

  const selection = activeObject as fabric.ActiveSelection;

  const sharkName = selection._objects.find(obj => obj.name?.startsWith('sharkName'));
  const sharkShortName = selection._objects.find(obj => obj.name?.startsWith('sharkShortName'));
  if (selection._objects.length === 2 && sharkName instanceof fabric.Text && sharkShortName instanceof fabric.Text) {
    canvasRef.current.remove(sharkName);
    canvasRef.current.remove(sharkShortName);

    if (sharkName instanceof fabric.Text && sharkShortName instanceof fabric.Text) {
      const sharkNameText = sharkName.text || '';
      const sharkNameName = sharkName.name || '';
  
      canvasRef.current.remove(sharkName);
      canvasRef.current.requestRenderAll();
  
      const newSharkName = new fabric.Text(sharkNameText, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2 +35,
        originX: 'right',
        originY: 'center',
        textAlign: 'center',
        backgroundColor: '#000000',
        fill: '#ffffff',
        name: sharkNameName,
        fontFamily: 'big_noodle_titling',
        fontSize: 28,
      });
      canvasRef.current.add(newSharkName);

      const sharkShortNameText = sharkShortName.text || '';
      const sharkShortNameName = sharkShortName.name || '';
      
      canvasRef.current.remove(sharkShortName);
      canvasRef.current.requestRenderAll();

      const newSharkShortName = new fabric.Text(sharkShortNameText, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'right',
        originY: 'center',
        textAlign: 'center',
        backgroundColor: '#000000',
        fill: '#ffffff',
        name: sharkShortNameName,
        fontFamily: 'big_noodle_titling',
      });
      canvasRef.current.add(newSharkShortName);

      const texts = new fabric.Group([newSharkName, newSharkShortName]);
      const backgroundRect = new fabric.Rect({
        width: (texts.width || 0) + 20,
        height: (texts.height || 0) + 30,
        fill: '#000000',
        originY: 'top',
        top: texts.getBoundingRect().top - 10,
        left: texts.getBoundingRect().left - 10,
        selectable: false,
        evented: false,
      });

      texts.addWithUpdate(backgroundRect);
      backgroundRect.moveTo(0);
      canvasRef.current?.add(texts);
      canvasRef.current?.renderAll();
      canvasRef.current?.setActiveObject(texts);

      canvasRef.current.remove(newSharkName);
      canvasRef.current.remove(newSharkShortName);
      canvasRef.current.remove(backgroundRect);
      canvasRef.current.renderAll();
    }
  } else {
    (activeObject as fabric.ActiveSelection).toGroup();
    canvasRef.current.requestRenderAll();
  }
};

export const handleUngroupObjects = (canvasRef: React.MutableRefObject<fabric.Canvas | null>) => {
  if (!canvasRef.current) return;

  const activeObject = canvasRef.current.getActiveObject();
  if (!activeObject || activeObject.type !== 'group') {
    return;
  }

  (activeObject as fabric.Group).toActiveSelection();
  canvasRef.current.requestRenderAll();

  const activeObjectAfter = canvasRef.current.getActiveObjects();
  activeObjectAfter.forEach((obj) => {
    if (obj instanceof fabric.Rect) {
      canvasRef.current?.remove(obj);
      canvasRef.current?.renderAll();
    }
  })

  canvasRef.current.discardActiveObject();
  canvasRef.current?.renderAll();
};

export const handleTextColorChange = (color: string, canvasRef: React.MutableRefObject<fabric.Canvas | null>) => {
  if (!canvasRef.current) return;

  const setColor = (obj: fabric.Object) => {
    if (obj instanceof fabric.Text) 
      obj.set('fill', color);
  };

  const setColorRecursive = (objects: fabric.Object[] | undefined) => {
    if (!objects) return;

    objects.forEach((obj) => {
      setColor(obj);

      if (obj instanceof fabric.Group) 
        setColorRecursive(obj.getObjects());
    });
  };

  const activeObjects = canvasRef.current.getActiveObjects();
  setColorRecursive(activeObjects);

  const activeObject = canvasRef.current.getActiveObject();
  if (activeObject instanceof fabric.Text) 
    activeObject.set('fill', color);

  canvasRef.current.renderAll();
};

import React, { useState, useRef } from 'react';
import FabricCanvas from './components/FabricCanvas';
import EventName from './components/EventName';
import GameType from './components/GameType';
import Race from './components/Race';
import DateInput from './components/DateInput';
import Arena from './components/Arena';
import SharkName from './components/SharkName';
import SharkShortName from './components/SharkShortName';
import SharkImage from './components/SharkImage';
import { isObjectExists, updateCanvasText, updateSharkIndex } from './helpers/Helpers';
import { handleBringToFront, handleGroupObjects, handleSendToBack, handleUngroupObjects } from './helpers/ObjectControllers';
import { fabric } from 'fabric';

function App() {
  const canvasRef = useRef<fabric.Canvas | null>(null);
  const [eventName, setEventName] = useState<string>('');
  const [gameType, setGameType] = useState<string>('');
  const [race, setRace] = useState<string>('');
  const [date, setDate] = useState<string>('');
  const [displayDate, setDisplayDate] = useState<string>('');
  const [arena, setArena] = useState<string>('');
  const [sharks, setSharks] = useState<Array<{ sharkName: string; sharkImage: File | null; sharkShortName: string }>>([{ sharkName: '', sharkImage: null, sharkShortName: '' }]);

  const toggleTextBox = () => {
    setSharks((prevSharks) => [
      ...prevSharks,
      { sharkName: '', sharkImage: null, sharkShortName: '' },
    ]);
  };

  const handleSharkImageChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    if (e.target.files && e.target.files[0]) {
      const file = e.target.files[0];
      
      setSharks((prevSharks) =>
        prevSharks.map((shark, i) =>
          i === index ? { ...shark, sharkImage: file } : shark
        )
      );

      if (canvasRef.current) {
        const sharkImageObj = canvasRef.current.getObjects().find((obj) => obj.name === `sharkImage${index}`);

        if (sharkImageObj instanceof fabric.Image) {
          canvasRef.current.remove(sharkImageObj);
        }

        fabric.Image.fromURL(URL.createObjectURL(file), (image) => {
          const canvasWidth = canvasRef.current?.width || 0;
          const canvasHeight = canvasRef.current?.height || 0;
          const scaleFactor = canvasHeight * (3 / 4) / (image.height || 0);

          // Set the scale factor while maintaining the aspect ratio
          const newWidth = (image.width || 0) * scaleFactor;
          const newHeight = (image.height || 0) * scaleFactor;
          const left = (canvasWidth - newWidth) / 2;
          const top = (canvasHeight - newHeight) / 2;

          image.set({
            name: `sharkImage${index}`,
            left: left,
            top: top,
            scaleX: newWidth / (image.width || 1),
            scaleY: newHeight / (image.height || 1),
          });
  
          canvasRef.current?.add(image);
          canvasRef.current?.renderAll();
        });
      }
    }
  };

  // =========== SHARK NAME =========== //

  const handleSharkNameChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    setSharks((prevSharks) =>
      prevSharks.map((shark, i) =>
        i === index ? { ...shark, sharkName: e.target.value } : shark
      )
    );

    const textObj = canvasRef.current?.getObjects().find((obj) => obj.name === 'sharkName' + index.toString());
    if (textObj instanceof fabric.Text) {
      textObj.set({text: e.target.value});
      canvasRef.current?.renderAll();
    } 
  };

  const handleAddSharkName = (e: React.KeyboardEvent, index: number) => {
    if (!canvasRef.current) return;

    const isExistingObj = canvasRef.current.getObjects().some((obj) => obj.name === `sharkName${index}`);
    if (!isExistingObj) {
      const textObject = new fabric.Text(sharks[index].sharkName, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: `sharkName${index}`,
        fontWeight: 'bold',
        fontFamily: 'big_noodle_titling',
      });

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  // =========== SHARK SHORT NAME =========== //

  const handleSharkShortNameChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    setSharks((prevSharks) =>
      prevSharks.map((shark, i) =>
        i === index ? { ...shark, sharkShortName: e.target.value } : shark
      )
    );

    const textObj = canvasRef.current?.getObjects().find((obj) => obj.name === 'sharkShortName' + index.toString());
    if (textObj instanceof fabric.Text) {
      textObj.set({text: e.target.value});
      canvasRef.current?.renderAll();
    } 
  };

  const handleAddSharkShortName = (e: React.KeyboardEvent, index: number) => {
    if (!canvasRef.current) return;

    const isExistingObj = canvasRef.current.getObjects().some((obj) => obj.name === `sharkShortName${index}`);
    if (!isExistingObj) {
      const textObject = new fabric.Text(sharks[index].sharkShortName, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: `sharkShortName${index}`,
        fontFamily: 'big_noodle_titling',
      });

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };
  
  // =========== SHARK DELETE =========== //

  const handleSharkDelete = (index: number) => {
    setSharks((prevSharks) => prevSharks.filter((sharkObj, i) => i !== index));

    if (!canvasRef.current) return;

    const sharkNameObj = canvasRef.current.getObjects().find((obj) => obj.name === `sharkName${index}`);
    const sharkImageObj = canvasRef.current.getObjects().find((obj) => obj.name === `sharkImage${index}`);
    const sharkShortNameObj = canvasRef.current.getObjects().find((obj) => obj.name === `sharkShortName${index}`);

    if (sharkNameObj) canvasRef.current.remove(sharkNameObj);
    if (sharkShortNameObj) canvasRef.current.remove(sharkShortNameObj);
    if (sharkImageObj) canvasRef.current.remove(sharkImageObj);

    // Update shark objects (eg., sharkName<index>) based on the updated array
    updateSharkIndex(canvasRef, 'sharkName', index);
    updateSharkIndex(canvasRef, 'sharkShortName', index);
    updateSharkIndex(canvasRef, 'sharkImage', index);

    canvasRef.current.discardActiveObject();
    canvasRef.current.renderAll();
  };

  // =========== EVENT NAME =========== //

  // Add new eventName
  const handleAddEventName = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;
    
    const isExistingObj = isObjectExists(canvasRef.current, eventName);
    if (!isExistingObj) {
      const textObject = new fabric.Text(eventName, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'eventName',
        fontWeight: 'bold',
        fontFamily: 'big_noodle_titling',
      });

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  // eventName onChange handler
  const handleEventNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEventName(e.target.value);
    updateCanvasText(canvasRef, e.target.value, 'eventName')
  };

  // =========== GAME TYPE =========== //

  const handleAddGameType = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;
    
    const isExistingObj = isObjectExists(canvasRef.current, gameType.toUpperCase());
    if (!isExistingObj) {
      const textObject = new fabric.Text(gameType.toUpperCase(), {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'gameType',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });
      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  const handleGameTypeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateCanvasText(canvasRef, e.target.value.toUpperCase(), 'gameType')
    setGameType(e.target.value);
  };

  // =========== RACE =========== //

  const handleAddRace = (e: React.KeyboardEvent) => {
    if (!canvasRef.current) return;
    
    const isExistingObj = isObjectExists(canvasRef.current, 'RACE TO ' + race);
    if (!isExistingObj) {
      const textObject = new fabric.Text(`RACE TO ${race.toUpperCase()}`, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'race',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });

      canvasRef.current.add(textObject);
      canvasRef.current.renderAll();
    }
  };

  const handleRaceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    updateCanvasText(canvasRef, `RACE TO ${e.target.value.toUpperCase()}`, 'race');
    setRace(e.target.value);

    if (e.target.value.length === 0) {
      updateCanvasText(canvasRef, `RACE TO ${e.target.value.toUpperCase()}`, 'race');
      setRace('');
    }
  };

  // =========== DATE =========== //

  const formatCustomDate = (dateString: string) => {
    const dateObj = new Date(dateString);
    return dateObj.toLocaleDateString(undefined, { weekday: 'long', month: 'long', day: 'numeric', year: 'numeric' }).toUpperCase();
  };

  const handleAddDate = () => {
    if (!canvasRef.current) return;
    
    const formattedDate = formatCustomDate(date);
    const isExistingObj = isObjectExists(canvasRef.current, formattedDate);

    if (!isExistingObj) {
      const dateObject = new fabric.Text(formattedDate, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'date',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });

      canvasRef.current.add(dateObject);
      canvasRef.current.renderAll();
    }
  };

  const handleDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDate(formatCustomDate(e.target.value));
    setDisplayDate(e.target.value);
    updateCanvasText(canvasRef, formatCustomDate(e.target.value), 'date');
  };

  // =========== ARENA =========== //

  const handleArenaSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const isExistingObj = isObjectExists(canvasRef.current, arena);
    setArena(e.target.value);
    updateCanvasText(canvasRef, e.target.value, 'arena');
    
    if (isExistingObj) return;

    if (e.target.value && canvasRef.current) {
      const itemObject = new fabric.Text(e.target.value, {
        left: (canvasRef.current.width || 0) / 2,
        top: (canvasRef.current.height || 0) / 2,
        originX: 'center',
        originY: 'center',
        textAlign: 'center',
        name: 'arena',
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'Century Gothic',
      });

      canvasRef.current.add(itemObject);
      canvasRef.current.renderAll();
    }
  };

  return (
    <div>
      <div className='row d-flex justify-content-center'>
        <div className='col-md-8 pe-0'>
          <FabricCanvas canvasRef={canvasRef} />
        </div>
        <div className='col-md-2 d-flex align-items-start justify-content-center flex-column'>
          <button className='btn btn-primary mb-2' onClick={(e) => handleBringToFront(canvasRef)}>Bring To Front</button>
          <button className='btn btn-primary mb-2' onClick={(e) => handleSendToBack(canvasRef)}>Send To Back</button>
          <button className='btn btn-primary mb-2' onClick={(e) => handleGroupObjects(canvasRef)}>Group</button>
          <button className='btn btn-primary mb-2' onClick={(e) => handleUngroupObjects(canvasRef)}>Ungroup</button>
        </div>
      </div>
      
      <div className='container w-'>
        <div className='row mb-2 d-flex align-items-start text-start'>
          <EventName
            eventName={eventName}
            handleEventNameChange={handleEventNameChange}
            handleAddEventName={handleAddEventName}
          />
          <GameType
            gameType={gameType}
            handleGameTypeChange={handleGameTypeChange}
            handleAddGameType={handleAddGameType}
          />
          <Race
            race={race}
            handleRaceChange={handleRaceChange}
            handleAddRace={handleAddRace}
          />
          <DateInput
            date={displayDate}
            handleDateChange={handleDateChange}
            handleAddDate={handleAddDate}
          />
          <Arena
            arena={arena}
            handleArenaSelect={handleArenaSelect}
          />
        </div>
        {sharks.map((shark, index) => (
          <div className='row d-flex text-start mb-3'>
            <div className='col-1 d-flex align-items-center justify-content-center'>
              <button className='btn' onClick={(e) => handleSharkDelete(index)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-x-circle-fill" viewBox="0 0 16 16">
                  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                </svg>
            </button>
            </div>
            <div className="col d-flex align-items-end">
              <SharkImage
                key={index}
                index={index}
                handleSharkImageChange={(e) => handleSharkImageChange(e, index)}
              />
              <div className='col d-flex align-items-start p-2 small'>{sharks[index].sharkImage?.name ? sharks[index].sharkImage?.name : 'No image uploaded'}</div>
            </div>
            <div className='col'>
              <SharkName
                key={index}
                index={index}
                name={shark ? shark.sharkName : ''}
                handleSharkNameChange={(e) => handleSharkNameChange(e, index)}
                handleAddSharkName={(e) => handleAddSharkName(e, index)}
              />
            </div>
            <div className="col">
              <SharkShortName
                key={index}
                index={index}
                name={shark ? shark.sharkShortName : ''}
                handleSharkShortNameChange={(e) => handleSharkShortNameChange(e, index)}
                handleAddSharkShortName={(e) => handleAddSharkShortName(e, index)}
              />
            </div>
          </div>
        ))}
        <button className='btn btn-primary' onClick={toggleTextBox}>Add More Shark</button>
      </div>
    </div>
  );
};

export default App